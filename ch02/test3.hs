type Count = Int
processingString :: String -> Count
processingString = undefined

data Compass = North | East | South | West
  deriving (Eq, Ord, Enum, Show)

-- instance Show Compass where
--   show North = "North"
--   show East = "East"
--   show South = "South"
--   show West = "West"

-- instance Eq Compass where
--   North == North = True

data Expression = Number Int
                | Add Expression Expression
                | Subtract Expression Expression
                deriving (Eq, Ord, Show)
