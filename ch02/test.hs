main :: IO ()
-- main = putStrLn "Hello from Haskell!"
main = putStrLn (greet("World"))

-- greeting = "Hello"
greeting = "Howdy"
greet who = greeting ++ ", " ++ who ++ "!"
